#include <stdio.h>
// -------------
// STRUCTS
// -------------


// -------------
// FUNCTIONS
// -------------
void showMainMenu();
void lookupCandidate();
void viewElectionResults();

// -------------
// Main function
// -------------
int main() {

    // 1. open the file
    FILE * inputFile = fopen("votedata.txt","r");
    if (inputFile == NULL) {
        printf("Cannot open file.\n");
        return -1;
    }
    else {
        printf("Opened file successfully.\n");
    }

    // 2. process the file

    // 3. create an array of candidates

    // 4. show menu
    int userChoice = 0;
    while (userChoice != -1) {
        showMainMenu();
        scanf("%d", &userChoice);

        if (userChoice == 1) {
            lookupCandidate();
        }
        else {
            viewElectionResults();
        }
    }
    printf("Done!");
    return 0;
}

void showMainMenu() {
    printf("VOTING RESULTS MENU:\n");
    printf("1. Lookup a candidate\n");
    printf("2. See election result\n");
    printf("Enter your choice (-1 to quit)\n");
}

void lookupCandidate() {
    printf("Candidate Lookup\n");
    printf("-----------------\n");
}

void viewElectionResults() {
    printf("View Election Results\n");
    printf("-----------------\n");
}
